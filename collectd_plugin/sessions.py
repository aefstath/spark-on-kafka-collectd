import collectd
import utmpaccess
import utmp
from UTMPCONST import *
import time
from netaddr import *


def read_func():


        number2=0
        number=0
        a = utmp.UtmpRecord()


        for b in a:
                if b.ut_type == USER_PROCESS:
                        (b.ut_user, b.ut_line, b.ut_pid,
                        b.ut_host, time.ctime(b.ut_tv[0]))
                        host=b.ut_host
                        if "cern.ch" in host:
                                number2 = number2+1
                        if "localhost" in host:
                                number2 = number2+1

                number=number+1
        total_sessions=int(number)
        in_cern_sessions=int(number2)
        #print number2
        #print number
        a.endutent()

        datapoint = collectd.Values(plugin='sessions',)
        datapoint.type = 'count'
        datapoint.type_instance = 'total_sessions'
        datapoint.values = [total_sessions]
        datapoint.dispatch()

        datapoint = collectd.Values(plugin='sessions',)
        datapoint.type = 'count'
        datapoint.type_instance = 'in_cern_sessions'
        datapoint.values = [ in_cern_sessions ]
        datapoint.dispatch()


collectd.register_read(read_func)
