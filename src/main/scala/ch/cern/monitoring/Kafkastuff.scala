import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.Row
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}

object Kafkastuff {

  def main(args: Array[String]) {
    val sparkConf = new SparkConf()
    sparkConf.setMaster("local[*]")

    val spark = SparkSession
      .builder
      .appName("Kafkastuff")
      .config(sparkConf)
      .getOrCreate()

    import spark.implicits._

    val kafkaData =
      spark
	.read
	.format("kafka")
        .option("kafka.bootstrap.servers", "monit-kafka.cern.ch:9092")
        .option("subscribe", "collectd_raw_users")
        .option("startingOffsets", "earliest")
        .load()
        .select($"value".cast("string")).rdd
        .map(row => row.getAs[String]("value"))

    val df= spark.read.json(kafkaData)
        .select($"data.*",$"metadata.*")

    df.show(false)


   // kafkaData.show(false)
       //.write
       //.format("console")
       //.option()
  }
}
